package response

// Response is the model for a Response.
type Response struct {
	Success bool   `json:"success"`
	Message string `json:"message,omitempty"`
}
