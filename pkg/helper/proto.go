package helper

import (
	"encoding/json"
	"fmt"

	"github.com/golang/protobuf/ptypes/any"
	"github.com/golang/protobuf/ptypes/wrappers"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/anypb"
)

// ConvertInterfaceToAny converts a generic interface to a protobuf Any type.
func ConvertInterfaceToAny(v interface{}) (*any.Any, error) {
	anyValue := &any.Any{}
	bytes, _ := json.Marshal(v)
	bytesValue := &wrappers.BytesValue{
		Value: bytes,
	}
	err := anypb.MarshalFrom(anyValue, bytesValue, proto.MarshalOptions{})
	return anyValue, err
}

// ConvertAnyToInterface converts a protobuf Any type to a generic interface.
func ConvertAnyToInterface(anyValue *any.Any) (interface{}, error) {
	var value interface{}
	bytesValue := &wrappers.BytesValue{}
	err := anypb.UnmarshalTo(anyValue, bytesValue, proto.UnmarshalOptions{})
	if err != nil {
		return value, err
	}
	uErr := json.Unmarshal(bytesValue.Value, &value)
	if err != nil {
		return value, uErr
	}

	return value, nil
}

// ConvertAnyToTargetInterface converts a protobuf Any type to a generic interface.
// The target interface is passed as a pointer.
func ConvertAnyToTargetInterface(anyValue *any.Any, target interface{}) error {
	if anyValue == nil {
		return fmt.Errorf("input 'Any' is nil")
	}

	// Convert Any to JSON bytes
	bytes, err := proto.Marshal(anyValue)
	if err != nil {
		return fmt.Errorf("error converting Any to JSON: %v", err)
	}

	// Unmarshal JSON bytes to target interface
	err = json.Unmarshal(bytes, target)
	if err != nil {
		return fmt.Errorf("error unmarshaling JSON to target interface: %v", err)
	}

	return nil
}
