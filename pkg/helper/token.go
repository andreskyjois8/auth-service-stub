package helper

func TrimBearerPrefix(token string) string {
	// check if there's bearer prefix
	if len(token) > 7 && token[:7] == "Bearer " {
		return token[7:]
	}

	return token
}
