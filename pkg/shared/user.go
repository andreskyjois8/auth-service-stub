package shared

import (
	"time"

	"github.com/google/uuid"
)

// User is the model for a User.
type User struct {
	ID              uuid.UUID            `json:"id"`
	Email           string               `json:"email"`
	EmailVerifiedAt *time.Time           `json:"email_verified_at"`
	Password        string               `json:"password,omitempty"`
	CreatedAt       *time.Time           `json:"created_at"`
	UpdatedAt       *time.Time           `json:"updated_at"`
	Classifications []UserClassification `json:"classifications"`
}

// UserClassification is the model for a UserClassification.
type UserClassification struct {
	ID                 uuid.UUID   `json:"id"`
	ClassificationName string      `json:"classification_name"`
	IsSuperAdmin       bool        `json:"is_super_admin,omitempty"`
	CreatedAt          *time.Time  `json:"created_at"`
	UpdatedAt          *time.Time  `json:"updated_at"`
	AdditionalInfo     interface{} `json:"additional_info"`
}

type UserWithoutPassword struct {
	ID              uuid.UUID            `json:"id"`
	Email           string               `json:"email"`
	EmailVerifiedAt *time.Time           `json:"email_verified_at"`
	CreatedAt       *time.Time           `json:"created_at"`
	UpdatedAt       *time.Time           `json:"updated_at"`
	Classifications []UserClassification `json:"classifications"`
}
