package user

import (
	"context"

	"gitlab.com/andreskyjois8/auth-service-stub/user/mapper"
)

type UserClient interface {
	Register(ctx context.Context, req mapper.RegisterRequest) (*mapper.RegisterResponse, error)
	GetUserByEmail(ctx context.Context, email string) (*mapper.GetUserEmailResponse, error)
	ResetPassword(ctx context.Context, access_token string, req mapper.ResetPasswordRequest) (*mapper.ResetPasswordResponse, error)
}
