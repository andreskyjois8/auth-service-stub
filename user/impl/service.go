package impl

import (
	"context"

	userpb "gitlab.com/andreskyjois8/auth-service-proto/user"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/helper"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/response"
	"gitlab.com/andreskyjois8/auth-service-stub/user"
	"gitlab.com/andreskyjois8/auth-service-stub/user/mapper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type userClient struct {
	r userpb.UserClient
}

// NewUserClient creates a new user client.
func NewUserClient(conn *grpc.ClientConn) user.UserClient {
	r := userpb.NewUserClient(conn)

	return &userClient{
		r,
	}
}

// Register a new user.
func (c *userClient) Register(ctx context.Context, req mapper.RegisterRequest) (*mapper.RegisterResponse, error) {
	reqPb := &userpb.RegisterRequest{
		Email:                req.Email,
		Password:             req.Password,
		PasswordConfirmation: req.PasswordConfirmation,
		UseOldPassword:       req.UseOldPassword,
		Classification:       req.Classifications,
	}

	regResponse, err := c.r.Register(ctx, reqPb)
	if err != nil {
		return nil, err
	}

	rsp := &mapper.RegisterResponse{
		Response: response.Response{
			Success: regResponse.GetSuccess(),
			Message: regResponse.GetMessage(),
		},
		UserId: regResponse.GetUserId(),
	}

	return rsp, nil
}

// Get User By Email.
// It returns a GetUserEmailResponse with the user.
// If the user is not found, it returns an error.
// If the user is found, it returns a GetUserEmailResponse with the user.
func (c *userClient) GetUserByEmail(ctx context.Context, email string) (*mapper.GetUserEmailResponse, error) {
	reqPb := &userpb.GetUserByEmailRequest{
		Email: email,
	}

	userResponse, err := c.r.GetUserByEmail(ctx, reqPb)
	if err != nil {
		return nil, err
	}

	user, err := mapper.ToUserWithoutPassword(userResponse)
	if err != nil {
		return nil, err
	}

	rsp := &mapper.GetUserEmailResponse{
		Response: response.Response{
			Success: userResponse.GetSuccess(),
			Message: userResponse.GetMessage(),
		},
		User: user,
	}

	return rsp, nil
}

func (c *userClient) ResetPassword(ctx context.Context, access_token string, req mapper.ResetPasswordRequest) (*mapper.ResetPasswordResponse, error) {
	access_token = helper.TrimBearerPrefix(access_token)
	ctx = metadata.NewOutgoingContext(ctx, metadata.Pairs("Authorization", "Bearer "+access_token))

	reqPb := &userpb.ResetPasswordRequest{
		OldPassword:             req.OldPassword,
		NewPassword:             req.NewPassword,
		NewPasswordConfirmation: req.NewPasswordConfirmation,
	}

	resetResponse, err := c.r.ResetPassword(ctx, reqPb)
	if err != nil {
		return nil, err
	}

	return &mapper.ResetPasswordResponse{
		Success: resetResponse.GetSuccess(),
		Message: resetResponse.GetMessage(),
	}, nil
}
