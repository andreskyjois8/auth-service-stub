package mapper

type RegisterRequest struct {
	Email                string   `json:"email"`
	Password             string   `json:"password"`
	PasswordConfirmation string   `json:"password_confirmation"`
	UseOldPassword       bool     `json:"use_old_password"`
	Classifications      []string `json:"classification"`
}

type ResetPasswordRequest struct {
	OldPassword             string `json:"old_password"`
	NewPassword             string `json:"new_password"`
	NewPasswordConfirmation string `json:"new_password_confirmation"`
}
