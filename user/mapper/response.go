package mapper

import (
	"encoding/json"

	userpb "gitlab.com/andreskyjois8/auth-service-proto/user"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/helper"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/response"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/shared"
)

type RegisterResponse struct {
	response.Response
	UserId string `json:"user_id"`
}

type GetUserEmailResponse struct {
	response.Response
	User *shared.UserWithoutPassword `json:"user,omitempty"`
}

func ToUserWithoutPassword(req *userpb.GetUserByEmailResponse) (*shared.UserWithoutPassword, error) {
	userIntf, err := helper.ConvertAnyToInterface(req.GetUser())
	if err != nil {
		return nil, err
	}

	userByte, err := json.Marshal(userIntf)
	if err != nil {
		return nil, err
	}

	var user shared.UserWithoutPassword
	err = json.Unmarshal(userByte, &user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

type ResetPasswordResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}
