module gitlab.com/andreskyjois8/auth-service-stub

go 1.21.3

require (
	github.com/golang/protobuf v1.5.3
	github.com/google/uuid v1.5.0
	github.com/stretchr/testify v1.8.4
	gitlab.com/andreskyjois8/auth-service-proto v0.0.6
	google.golang.org/grpc v1.60.0
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.16.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20231002182017-d307bd883b97 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
