package authservicestub

import (
	"context"
	"fmt"

	"gitlab.com/andreskyjois8/auth-service-stub/auth"
	authImpl "gitlab.com/andreskyjois8/auth-service-stub/auth/impl"
	"gitlab.com/andreskyjois8/auth-service-stub/user"
	userImpl "gitlab.com/andreskyjois8/auth-service-stub/user/impl"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Client interface {
	Close() error
	NewAuthClient() (auth.AuthClient, error)
	NewUserClient() (user.UserClient, error)
}

type client struct {
	conn *grpc.ClientConn
}

// Initialize a new client.
func NewClient(ctx context.Context, host string, port string) (Client, error) {
	conn, err := grpc.DialContext(ctx, fmt.Sprintf("%s:%s", host, port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	return &client{
		conn,
	}, nil
}

// Close the client connection.
func (c *client) Close() error {
	return c.conn.Close()
}

// Create a new auth client for Communication with the Auth Module (Login, Logout, Me).
func (c *client) NewAuthClient() (auth.AuthClient, error) {
	authClient := authImpl.NewAuthClient(c.conn)

	return authClient, nil
}

// Create a new user client for Communication with the User Module (Register).
func (c *client) NewUserClient() (user.UserClient, error) {
	userClient := userImpl.NewUserClient(c.conn)

	return userClient, nil
}
