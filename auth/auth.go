package auth

import (
	"context"

	"gitlab.com/andreskyjois8/auth-service-stub/auth/mapper"
)

// AuthClient is the interface for the auth client.
type AuthClient interface {
	Authenticate(ctx context.Context, req mapper.LoginRequest) (*mapper.LoginResponse, error)
	Me(ctx context.Context, access_token string) (*mapper.MeResponse, error)
	Logout(ctx context.Context, access_token string) (*mapper.LogoutResponse, error)
}
