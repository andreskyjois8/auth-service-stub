package mapper

import (
	"encoding/json"
	"errors"

	"gitlab.com/andreskyjois8/auth-service-stub/pkg/helper"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/response"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/shared"
	"google.golang.org/protobuf/types/known/anypb"
)

// AccessTokenData is the data for the access token.
type AccessTokenData struct {
	AccessToken string `json:"access_token"`
}

// LoginResponse is the response for the login request.
type LoginResponse struct {
	response.Response
	Data AccessTokenData `json:"data,omitempty"`
}

// MeResponse is the response for the me request.
type MeResponse struct {
	response.Response
	Data *shared.User `json:"data,omitempty"`
}

// LogoutResponse is the response for the logout request.
type LogoutResponse struct {
	response.Response
}

// Convert anypb.Any to shared.User.
// If the data is not a string, it returns an error.
// If the data is a string, it unmarshals the string to a shared.User.
// This is used to convert the user data from anypb.Any to shared.User.
// When method Me is called, it converts the user data from anypb.Any to shared.User.
func ToUserData(data *anypb.Any) (*shared.User, error) {
	userIntf, err := helper.ConvertAnyToInterface(data)
	if err != nil {
		return nil, err
	}

	userStringify, ok := userIntf.(string)
	if !ok {
		return nil, errors.New("user data is not a string")
	}

	var user shared.User

	err = json.Unmarshal([]byte(userStringify), &user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}
