package mapper

// Login Request to Authenticate
type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
