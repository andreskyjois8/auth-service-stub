package impl

import (
	"context"

	authpb "gitlab.com/andreskyjois8/auth-service-proto/auth"
	"gitlab.com/andreskyjois8/auth-service-stub/auth"
	"gitlab.com/andreskyjois8/auth-service-stub/auth/mapper"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/helper"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/response"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type authClient struct {
	r authpb.AuthClient
}

// NewAuthClient creates a new auth client.
func NewAuthClient(conn *grpc.ClientConn) auth.AuthClient {
	r := authpb.NewAuthClient(conn)
	return &authClient{
		r,
	}
}

// Authenticate is method for Authenticating a user/Login as user.
// It returns a LoginResponse with the access token.
// If the user is not found or the password is wrong, it returns an error.
// If the user is found and the password is correct, it returns a LoginResponse with the access token.
func (c *authClient) Authenticate(ctx context.Context, req mapper.LoginRequest) (*mapper.LoginResponse, error) {
	reqPb := &authpb.AuthRequest{
		Email:    req.Email,
		Password: req.Password,
	}

	authResponse, err := c.r.Authenticate(ctx, reqPb)
	if err != nil {
		return nil, err
	}

	rsp := &mapper.LoginResponse{
		Response: response.Response{
			Success: authResponse.GetSuccess(),
			Message: authResponse.GetMessage(),
		},
		Data: mapper.AccessTokenData{
			AccessToken: authResponse.AccessToken,
		},
	}

	return rsp, nil
}

// Me is method for getting the user data.
// It returns a MeResponse with the user data.
// If the user is not found, it returns an error.
// If the user is found, it returns a MeResponse with the user data.
// The access token must be passed in the param access_token as a Bearer token.
func (c *authClient) Me(ctx context.Context, access_token string) (*mapper.MeResponse, error) {
	access_token = helper.TrimBearerPrefix(access_token)
	ctx = metadata.NewOutgoingContext(ctx, metadata.Pairs("Authorization", "Bearer "+access_token))

	meResponse, err := c.r.Me(ctx, &authpb.MeRequest{})
	if err != nil {
		return nil, err
	}

	user, err := mapper.ToUserData(meResponse.Data)
	if err != nil {
		return nil, err
	}

	rsp := &mapper.MeResponse{
		Response: response.Response{
			Success: meResponse.GetSuccess(),
		},
		Data: user,
	}

	return rsp, nil
}

// Logout is method for logging out a user.
// It returns a LogoutResponse.
// The access token must be passed in the param access_token as a Bearer token.
// If the user is not found, it returns an error.
// If the user is found, it returns a LogoutResponse.
func (c *authClient) Logout(ctx context.Context, access_token string) (*mapper.LogoutResponse, error) {
	access_token = helper.TrimBearerPrefix(access_token)
	ctx = metadata.NewOutgoingContext(ctx, metadata.Pairs("Authorization", "Bearer "+access_token))

	logoutResponse, err := c.r.Logout(ctx, &authpb.LogoutRequest{})
	if err != nil {
		return nil, err
	}

	rsp := &mapper.LogoutResponse{
		Response: response.Response{
			Success: logoutResponse.GetSuccess(),
		},
	}

	return rsp, nil
}
