package main

import (
	"context"
	"log"
	"os"

	authservicestub "gitlab.com/andreskyjois8/auth-service-stub"
	"gitlab.com/andreskyjois8/auth-service-stub/auth/mapper"
)

func main() {
	// get env vars
	host := os.Getenv("RPC_HOST")
	port := os.Getenv("RPC_PORT")

	// get env vars for login request for testing purposes
	email := os.Getenv("LOGIN_EMAIL")
	password := os.Getenv("LOGIN_PASSWORD")

	// create client
	ctx := context.Background()
	client, err := authservicestub.NewClient(ctx, host, port)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()

	// create auth client
	authClient, err := client.NewAuthClient()
	if err != nil {
		log.Fatal(err)
	}

	// create login request
	loginRequest := mapper.LoginRequest{
		Email:    email,
		Password: password,
	}

	// call authenticate method
	loginResponse, err := authClient.Authenticate(ctx, loginRequest)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("access token", loginResponse.Data.AccessToken)

	// call me method
	meResponse, err := authClient.Me(ctx, loginResponse.Data.AccessToken)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("user id", meResponse.Data.ID)

	// call logout method
	logoutResponse, err := authClient.Logout(ctx, loginResponse.Data.AccessToken)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Is Logout ?", logoutResponse.Success)
}
