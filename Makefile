.PHONY: test-all example

test-all:
	LOGIN_EMAIL=jois.andresky@cognotiv.com LOGIN_PASSWORD=password RPC_HOST=localhost RPC_PORT=9301 go test -v ./tests/...

example:
	LOGIN_EMAIL=jois.andresky@cognotiv.com LOGIN_PASSWORD=password RPC_HOST=localhost RPC_PORT=9301 go run ./example/main.go

publish:
	@read -p "Enter new version: " NEW_VERSION && \
	read -p "Enter tag message: " TAG_MESSAGE && \
	OLD_VERSION=$$(git describe --tags --abbrev=0) && \
	if [ "$$OLD_VERSION" != "" ]; then \
		echo "Deleting existing tag: $$OLD_VERSION" && \
		git tag -d $$OLD_VERSION && \
		git push --delete origin refs/tags/$$OLD_VERSION; \
	fi && \
	git tag -a $$NEW_VERSION -m "$$TAG_MESSAGE" && \
	git push origin refs/tags/$$NEW_VERSION