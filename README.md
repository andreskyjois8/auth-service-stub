# Project Description

This is the Package wrapper to Communicate with GRPC Auth Service.


# Install

    go get gitlab.com/andreskyjois8/auth-service-stub

## Usage

1. Initialize Client
```go
import (
	"context"
	"log"
	"os"

	authservicestub "gitlab.com/andreskyjois8/auth-service-stub"
)

func main() {
    // get env vars
    host := os.Getenv("RPC_HOST")
    port := os.Getenv("RPC_PORT")

    // create client
    ctx := context.Background()
    client, err := authservicestub.NewClient(ctx, host, port)
    if err !=  nil {
        log.Fatal(err)
    }
    defer client.Close()
}
```

2. Initialize AuthClient with Client object
```go
/// previous import

func main() {
    /// ... PREVIOUS CODE
    
    // create auth client
    authClient, err := client.NewAuthClient()
    if err != nil {
        log.Fatal(err)
    }
}
```

3. Example Usage to Authenticate, Get User Info and Logout
```go
import (
	/// previous imports
	"gitlab.com/andreskyjois8/auth-service-stub/auth/mapper"
)

func main() {

    /// ...PREVIOUS CODE

    // create login request
    loginRequest := mapper.LoginRequest{
        Email:    email,
        Password: password,
    }
    
    // call authenticate method
    loginResponse, err := authClient.Authenticate(ctx, loginRequest)
    if err != nil {
        log.Fatal(err)
    }
    
    log.Println("access token", loginResponse.Data.AccessToken)
    
    // call me method
    meResponse, err := authClient.Me(ctx, loginResponse.Data.AccessToken)
    if err != nil {
        log.Fatal(err)
    }
    
    log.Println("user id", meResponse.Data.ID)
    
    // call logout method
    logoutResponse, err := authClient.Logout(ctx, loginResponse.Data.AccessToken)
    if err != nil {
        log.Fatal(err)
    }
    
    log.Println("Is Logout ?", logoutResponse.Success)
}
```