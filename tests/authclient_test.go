package tests

import (
	"context"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	authservicestub "gitlab.com/andreskyjois8/auth-service-stub"
	authMapper "gitlab.com/andreskyjois8/auth-service-stub/auth/mapper"
	"gitlab.com/andreskyjois8/auth-service-stub/pkg/response"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestAuthClient_Login(t *testing.T) {
	host := os.Getenv("RPC_HOST")
	port := os.Getenv("RPC_PORT")

	email := os.Getenv("LOGIN_EMAIL")
	password := os.Getenv("LOGIN_PASSWORD")

	ctx := context.Background()
	client, err := authservicestub.NewClient(ctx, host, port)
	assert.NoError(t, err)

	authClient, err := client.NewAuthClient()
	assert.NoError(t, err)

	type expectation struct {
		out *authMapper.LoginResponse
		err error
	}

	tests := map[string]struct {
		in       authMapper.LoginRequest
		expected expectation
	}{
		"200-Authenticated": {
			in: authMapper.LoginRequest{
				Email:    email,
				Password: password,
			},
			expected: expectation{
				out: &authMapper.LoginResponse{
					Response: response.Response{
						Success: true,
						Message: "Authenticated",
					},
					Data: authMapper.AccessTokenData{
						AccessToken: "<KEY>",
					},
				},
				err: nil,
			},
		},
		"400-Invalid Email": {
			in: authMapper.LoginRequest{
				Email:    "invalid.email",
				Password: password,
			},
			expected: expectation{
				out: nil,
				err: status.Error(codes.InvalidArgument, "email: must be a valid email address."),
			},
		},
		"400-Invalid Password": {
			in: authMapper.LoginRequest{
				Email:    email,
				Password: "<PASSWORD>",
			},
			expected: expectation{
				out: nil,
				err: status.Error(codes.Unauthenticated, "invalid credentials, your password doesn't matched"),
			},
		},
		"400-User is Not Exist": {
			in: authMapper.LoginRequest{
				Email:    "jois.andresky@cognotiv.co",
				Password: password,
			},
			expected: expectation{
				out: nil,
				err: status.Error(codes.Unauthenticated, "failed to find user, user account is not exist!"),
			},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			rsp, err := authClient.Authenticate(context.Background(), tc.in)
			if err != nil {
				log.Println(err)
			}

			if rsp != nil {
				assert.Equal(t, tc.expected.out.Response, rsp.Response)
				assert.NotEmpty(t, rsp.Data.AccessToken)
			}

			assert.Equal(t, tc.expected.err, err)
		})
	}

	assert.NoError(t, client.Close())
}

func TestAuthClient_Me(t *testing.T) {
	host := os.Getenv("RPC_HOST")
	port := os.Getenv("RPC_PORT")

	email := os.Getenv("LOGIN_EMAIL")
	password := os.Getenv("LOGIN_PASSWORD")

	ctx := context.Background()
	client, err := authservicestub.NewClient(ctx, host, port)
	assert.NoError(t, err)

	authClient, err := client.NewAuthClient()
	assert.NoError(t, err)

	t.Run("200-Authenticated and User Info", func(t *testing.T) {
		authRsp, err := authClient.Authenticate(ctx, authMapper.LoginRequest{
			Email:    email,
			Password: password,
		})
		assert.NoError(t, err)

		if authRsp != nil {
			meRsp, err := authClient.Me(ctx, authRsp.Data.AccessToken)
			assert.NoError(t, err)

			if meRsp != nil {
				assert.NotEmpty(t, meRsp.Data.ID)
				assert.Equal(t, email, meRsp.Data.Email)
			}
		} else {
			t.Fail()
		}
	})

	assert.NoError(t, client.Close())
}

func TestAuthClient_Logout(t *testing.T) {
	host := os.Getenv("RPC_HOST")
	port := os.Getenv("RPC_PORT")

	email := os.Getenv("LOGIN_EMAIL")
	password := os.Getenv("LOGIN_PASSWORD")

	ctx := context.Background()
	client, err := authservicestub.NewClient(ctx, host, port)
	assert.NoError(t, err)

	authClient, err := client.NewAuthClient()
	assert.NoError(t, err)

	t.Run("200-Logout", func(t *testing.T) {
		authRsp, err := authClient.Authenticate(ctx, authMapper.LoginRequest{
			Email:    email,
			Password: password,
		})
		assert.NoError(t, err)

		if authRsp != nil {
			logoutRsp, err := authClient.Logout(ctx, authRsp.Data.AccessToken)
			assert.NoError(t, err)

			if logoutRsp != nil {
				assert.Equal(t, true, logoutRsp.Response.Success)
			}
		} else {
			t.Fail()
		}
	})

	assert.NoError(t, client.Close())
}
