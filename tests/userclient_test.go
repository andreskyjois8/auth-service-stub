package tests

import (
	"context"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	authservicestub "gitlab.com/andreskyjois8/auth-service-stub"
	authMapper "gitlab.com/andreskyjois8/auth-service-stub/auth/mapper"
	userMapper "gitlab.com/andreskyjois8/auth-service-stub/user/mapper"
)

func TestUserClient_Register(t *testing.T) {
	host := os.Getenv("RPC_HOST")
	port := os.Getenv("RPC_PORT")

	ctx := context.Background()
	client, err := authservicestub.NewClient(ctx, host, port)
	assert.NoError(t, err)

	userclient, err := client.NewUserClient()
	assert.NoError(t, err)

	t.Run("Register Is Working", func(t *testing.T) {
		req := userMapper.RegisterRequest{
			Email:                "something@gmail.com",
			Password:             "password",
			PasswordConfirmation: "password",
			UseOldPassword:       false,
			Classifications:      []string{"LEARNER"},
		}

		resp, err := userclient.Register(ctx, req)
		if err != nil {
			log.Println("Containing some error means that the test is working", err)
		}

		if resp != nil {
			log.Println("Containing some response means that the test is working")
			log.Println("Is Success", resp.Success)
			log.Println("Have a Message", resp.Message)
			log.Println("Have a Some Data", resp.UserId)
		}
	})

	assert.NoError(t, client.Close())
}

func TestUserClient_GetUserByEmail(t *testing.T) {
	host := os.Getenv("RPC_HOST")
	port := os.Getenv("RPC_PORT")

	ctx := context.Background()
	client, err := authservicestub.NewClient(ctx, host, port)
	assert.NoError(t, err)

	userclient, err := client.NewUserClient()
	assert.NoError(t, err)

	t.Run("GetUserByEmail Is Working", func(t *testing.T) {
		resp, err := userclient.GetUserByEmail(ctx, "something@gmail.com")
		if err != nil {
			log.Println("Containing some error means that the test is working: ", err)
		}

		if resp != nil {
			log.Println("Containing some response means that the test is working")
			log.Println("Is Success", resp.Success)
			log.Println("Have a Message", resp.Message)
			log.Println("Have a Some Data", resp.User)
		}
	})

	assert.NoError(t, client.Close())
}

func TestUserClient_ResetPassword(t *testing.T) {
	host := os.Getenv("RPC_HOST")
	port := os.Getenv("RPC_PORT")

	ctx := context.Background()
	client, err := authservicestub.NewClient(ctx, host, port)
	assert.NoError(t, err)

	authclient, err := client.NewAuthClient()
	assert.NoError(t, err)

	userclient, err := client.NewUserClient()
	assert.NoError(t, err)

	t.Run("ResetPassword Is Working", func(t *testing.T) {
		loginReq := authMapper.LoginRequest{
			Email:    "something@gmail.com",
			Password: "password",
		}

		loginRsp, err := authclient.Authenticate(ctx, loginReq)
		assert.NoError(t, err)

		if loginRsp != nil {
			log.Println("Containing some response means that the Login test is working")
			log.Println("Is Success", loginRsp.Success)
			log.Println("Have a Message", loginRsp.Message)
			log.Println("Have a Some Data", loginRsp.Data.AccessToken)

			req := userMapper.ResetPasswordRequest{
				OldPassword:             "password",
				NewPassword:             "password1",
				NewPasswordConfirmation: "password1",
			}

			resp, err := userclient.ResetPassword(ctx, loginRsp.Data.AccessToken, req)
			if err != nil {
				log.Println("Containing some error means that the test is working: ", err)
			}

			if resp != nil {
				log.Println("Containing some response means that the test is working")
				log.Println("Is Success", resp.Success)
				log.Println("Have a Message", resp.Message)
			}
		}
	})

	assert.NoError(t, client.Close())
}
